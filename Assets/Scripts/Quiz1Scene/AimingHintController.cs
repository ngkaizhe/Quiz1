﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimingHintController : MonoBehaviour
{
    public GameObject AimingTarget;
    public GameObject Ball;

    private int aimingHintRotateDir = 1;
    private Vector3 originalAimingTargetPosition;

    // Start is called before the first frame update
    void Start()
    {
        originalAimingTargetPosition = AimingTarget.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        float upperAngleBound = 45f;
        float lowerAngleBound = -upperAngleBound;
        float rotateSpeed = Time.deltaTime * 200f;

        float currentAngle = Vector3.SignedAngle(originalAimingTargetPosition - Ball.transform.position, AimingTarget.transform.position - Ball.transform.position, Vector3.up);

        if (aimingHintRotateDir == 1 && currentAngle > upperAngleBound)
        {
            aimingHintRotateDir = -1;
        }
        else if (aimingHintRotateDir == -1 && currentAngle < lowerAngleBound)
        {
            aimingHintRotateDir = 1;
        }

        gameObject.transform.Rotate(new Vector3(0, aimingHintRotateDir * rotateSpeed, 0), Space.World);
    }
}
