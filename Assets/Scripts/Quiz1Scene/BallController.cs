﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public ParticleSystem particle;
    public GameObject AimingTarget;
    public GameObject AimingHint;

    private Vector3 originalPosition;

    private bool shooted;
    private bool hasExploded;

    private float fixedTime;
    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        originalPosition = gameObject.transform.position;
        reset();
    }

    // Update is called once per frame
    void Update()
    {
        float speed = -50000;
        Rigidbody rigidbody = GetComponent<Rigidbody>();

        if (shooted)
        {
            timer += Time.deltaTime;
        }

        if(timer > fixedTime)
        {
            if (SameVectors(rigidbody.velocity, Vector3.zero) && !hasExploded)
            {
                ParticleSystem particleInstance = Instantiate(particle, this.transform.position, Quaternion.Euler(Vector3.zero));
                Destroy(particleInstance.gameObject, 10f);
                hasExploded = true;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && !shooted)
        {
            Vector3 dir = Vector3.Normalize(AimingTarget.transform.position - gameObject.transform.position);
            dir.y = 0;
            rigidbody.AddForce(dir * -speed);
            shooted = true;
            AimingHint.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            reset();
        }
    }

    bool SameVectors(Vector3 first, Vector3 second, float error = 0.5f)
    {
        float x = Mathf.Abs(first.x - second.x);
        float y = Mathf.Abs(first.y - second.y);
        float z = Mathf.Abs(first.z - second.z);
        return (x + y + z) < error;
    }

    private void reset()
    {
        fixedTime = 1.0f;
        timer = 0f;
        hasExploded = false;
        shooted = false;
        AimingHint.SetActive(true);
        gameObject.transform.position = originalPosition;

        Rigidbody rigidbody = GetComponent<Rigidbody>();
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;
    }
}
