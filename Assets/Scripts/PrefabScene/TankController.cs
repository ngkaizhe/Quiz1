﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankController : MonoBehaviour
{
    public GameObject towerHead;
    public GameObject bullet;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 up = new Vector3(0, 1, 0);
        Vector3 front = Vector3.Normalize(towerHead.transform.position - transform.position) * Time.deltaTime;
        front.y = 0;
        Vector3 right = Vector3.Cross(up, front).normalized * Time.deltaTime;
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(-right, Space.World); 
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(right, Space.World);
        }
        else if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(front, Space.World);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(-front, Space.World);
        }

        float rotationSpeed = -0.5f;
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(0, rotationSpeed, 0, Space.World);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(0, -rotationSpeed, 0, Space.World);
        }

        Vector3 towerHeadPos = towerHead.transform.position;
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject bulletObj = Instantiate(bullet, towerHeadPos, Quaternion.Euler(new Vector3(-90,0,0)));
            Rigidbody rigidbody = bulletObj.GetComponent<Rigidbody>();
            rigidbody.AddForce(front * 500);
            Destroy(bulletObj, 10f);
        }
        
    }
}
