﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit = new RaycastHit();
        Vector3 pos = Input.mousePosition;
        Ray mouseray = Camera.main.ScreenPointToRay(pos);

        if (Input.GetMouseButtonDown(0))
        {
            if(Physics.Raycast(mouseray, out hit))
            {
                Debug.Log(string.Format("You clicked {0}", hit.collider.gameObject.name));
            }
        }
    }
}
