﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeController: MonoBehaviour
{
    public int cubeNumber;
    public Transform targetTransform;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Rigidbody rigidbody = GetComponent<Rigidbody>();
        Vector3 dir = new Vector3(1,0,0);
   
        if (cubeNumber == 0)
        {
            rigidbody.velocity = dir;
        }
        else if (cubeNumber == 1)
        {
            float speed = 10;
            rigidbody.AddForce(dir * speed);
        }
        else if (cubeNumber == 2)
        {
            rigidbody.transform.Translate(dir * Time.deltaTime, Space.World);
        }
        else if (cubeNumber == 3)
        {
            rigidbody.transform.position += dir * Time.deltaTime;
        }
    }

}
